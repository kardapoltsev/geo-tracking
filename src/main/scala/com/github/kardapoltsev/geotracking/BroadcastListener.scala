package com.github.kardapoltsev.geotracking

import android.content.{BroadcastReceiver, Context, Intent}
import android.os.AsyncTask
import android.content.Intent.ACTION_BOOT_COMPLETED
import android.content.Intent.ACTION_MY_PACKAGE_REPLACED
import android.util.Log
import org.scaloid.common.SIntent;

class BroadcastListener extends BroadcastReceiver {
  def onReceive(context: Context, intent: Intent): Unit = {
    val action = intent.getAction
    Log.i("BroadcastListener", s"got intent with action $action")
    try handle(context, action)
    catch {
      case ex: IllegalStateException =>
        Log.e(getClass.getName, "Failed to handle action %s - retry in 10 seconds", ex)
        new Retry(context, action).execute()
    }
  }

  private def handle(context: Context, action: String): Unit = {
    if (ACTION_BOOT_COMPLETED.equals(action)) start(context)
    else if (ACTION_MY_PACKAGE_REPLACED.equals(action)) start(context)
  }

  private def start(implicit context: Context): Unit = {
    context.startForegroundService(SIntent[GeoTrackingService])
  }

  private class Retry(context: Context, a: String) extends AsyncTask[Void, Integer, Void] {
    override protected def doInBackground(args: Void*): Void = {
      try Thread.sleep(10000)
      catch {
        case ex: InterruptedException =>
          Log.e(getClass.getName, "Retry task interrupted", ex)
      }
      null
    }

    override protected def onPostExecute(aVoid: Void): Unit = {
      try {
        handle(context, a)
      } catch {
        case ex: IllegalStateException =>
          Log.e(getClass.getName, "Retry failed to handle action %s", ex)
      }
    }
  }

}
